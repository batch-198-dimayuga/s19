// console.log("Hello World");

let username = "";
let password = "";
let role = "";


function userDetails(){
    username = prompt("Enter your username: ").toLowerCase();
    password = prompt("Enter your password:").toLowerCase();
    role = prompt("Enter your role:").toLowerCase();

    if(username === "" || password === "" || role === ""){
        alert("Your input should not be empty");
    } else {
        switch(role){
            case 'admin':
                alert("Welcome back to the class portal, admin!");
                break;
            case 'teacher':
                alert("Thank you for logging in, teacher!");
                break;
            case 'rookie':
                alert("Welcome to the class portal, student!");
                break;
            default: 
                alert("Role out of range.");
                break;
        }
    }
}
userDetails();

function checkAverage(grade1,grade2,grade3,grade4){
    let average = Math.round((grade1 + grade2 + grade3 + grade4)/4);
    // console.log(average);
    
    if(average <= 74){
        console.log("Hello, student, your average is " + average + ". The letter equivalent is F");
    } else if (average >= 75 && average <= 79){
        console.log("Hello, student, your average is " + average + ". The letter equivalent is D");
    } else if (average >= 80 && average <= 84){
        console.log("Hello, student, your average is " + average + ". The letter equivalent is C");
    } else if (average >= 85 && average <= 89){
        console.log("Hello, student, your average is " + average + ". The letter equivalent is B");
    } else if (average >= 90 && average <= 95){
        console.log("Hello, student, your average is " + average + ". The letter equivalent is A");
    } else if (average >= 96){
        console.log("Hello, student, your average is " + average + ". The letter equivalent is A+");
    }
}      
checkAverage(70,70,72,71);
checkAverage(76,76,77,79);
checkAverage(81,83,84,85);
checkAverage(87,88,88,89);
checkAverage(91,90,92,90);
checkAverage(96,95,97,97);